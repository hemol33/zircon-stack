package com.haemol.zircon

import org.hexworks.zircon.api.{CP437TilesetResources, ColorThemes, Components, DrawSurfaces, SwingApplications}
import org.hexworks.zircon.api.application.AppConfig
import org.hexworks.zircon.api.data.{Position, Size, Tile}
import org.hexworks.zircon.api.graphics.TileGraphics
import org.hexworks.zircon.api.grid.TileGrid
import org.hexworks.zircon.api.screen.Screen

object Demo {
  def main(args: Array[String]): Unit = {
    val tileGrid: TileGrid = SwingApplications.startTileGrid(AppConfig.newBuilder
      .withSize(Size.create(60, 30))
      .withDefaultTileset(CP437TilesetResources.rexPaint16x16)
      .build)

    // A Screen is an abstraction which lets you use text GUI Components
    // You can have multiple Screens attached to the same TileGrid to be able to create multiple
    // screens for your app.
    val screen = Screen.create(tileGrid)

    // Creating text GUI Components is super simple
    val label = Components.label.withText("Hello, Zircon!").withPosition(Position.create(23, 1)).build

    // Screens can hold GUI components
    screen.addComponent(label)

    screen.display()

    val graphicPosition = Position.create(22, 4)
    val groundTile = Tile.newBuilder.withCharacter('.').build

    val graphics: TileGraphics = DrawSurfaces.tileGraphicsBuilder
      .withSize(15, 15)
      .withTileset(CP437TilesetResources.rexPaint16x16)
      .withFiller(groundTile)
      .build

    screen.draw(graphics, graphicPosition)
  }
}
