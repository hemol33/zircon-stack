package com.haemol.melee.zircon

import org.hexworks.zircon.api.data.Tile

object TileInstance {
  def tile(): Tile = {
    Tile.newBuilder.withCharacter("a").build
  }
}
