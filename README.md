# About

This repository is a showcase of a scala compiler
throwing a StackOverflowError when using [Zircon library](https://hexworks.org/projects/zircon/).

## Usage

```bash
./gradlew build
```

One can also run `Demo` class to see that the library can work.
